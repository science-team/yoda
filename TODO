YODA TODO list
==============

NOW
---

* Python docstrings for Points, Dbns and Bins (AB)

* Make Python interface test scripts (DM, AB)
   Test Histo2D and Scatter2D from Python

* Add copy assignment to both C++ and Cython (AB)

* Add stdErr to HistoBinXD (AB)

* Add a goodness of fit test collection (AB)
   Simple GoF measures, such as chi2 of a bin and of a histo (chained), for 1D
   and 2D Histos, Profiles and Scatters. Also KS (later)?

* Use better error than sqrt(sumW2()) in HistoBin
   Actually implement on Dbn0D? Should we have better separation of pure-w and wx**n?


NEXT
----

* Support reading of Counter from YODA files

* Map Counter in Cython

* Map Histo1D/2D and Profile1D/2D numEntries and effNumEntries in Cython

* Rename/alias Axis as Binning
   This is just a better name for what it does. Expose it for use with arbitrary
   contained types: doubles and Histos. Bin by string value (or arbitrary
   types... again?) Separate "bin-lookup" types with and without overflow
   capability?

* Add Scatter1D/Point1D and conversion/division/efficiency from Counter(s)

* Complete 2D histogramming

* Add Axis2D -> Histo2D/Profile2D bin adding and erasing. (AB)
   Adding 2D operator support and bin-adding/erasing

* 2D binning rectangular bin merges

* Automatic dynamic rebinning by sequential bin merging
   Like a jet algorithm: define a stat poorness measure and stopping
   condition... user-suppliable as function objects?

* Test negative- and mixed-weighted stat calculations, and scaling.


AND THEN...
-----------

* Remove non-const bin access from Histos and Profiles?
   cf. David D's point re. consistency via email on 7/8/2012
   In practice they can't be removed as they are used internally. Attempting to
   make them protected/private didn't work due to requirements of the Python
   wrapper code... leaving it as-is for now.

* 2D bin merging and rebinning (AB)
   Only rebin perfect grids: need to detect them. Global rebinning of Axis2D ->
   Histo2D by generally different integer factors in x and y directions.

* Test gap handling on 1D and 2D axes.

* Add Histo2D binning constructors from Scatter3D and Profile2D.

* Add Scatter1D/Point1D (AB)


AND AFTER THAT...

* Integer axis

* Allow comments on ends of YODA format data lines in ReaderYODA (HH)
   Ignore anything after "#" on data lines -- use this to write convenience
   height & error info for each bin since not obvious from sumWX2 etc.

* Multi-error support on Point types
   Access by index, with negative (enum) indices for quad/linear/largest combn?

* Scatter2D -> Scatter<N> again (?) with specialisations for 1D, 2D and 3D

* Terminal histos via WriterAsciiArt

* Write scripts to convert between data formats and to plot results.

* Template all classes on floating point type (default to double)

* Look into pickling of C++ interfaced classes. WriterS10n? MessagePack? Protobuf?

* Simple ntupling, done well :-)
   Surprisingly the Protobuf, MsgPack, etc. systems don't seem designed for
   ntuples... surely something is? Maybe wrap HDF5 or similar? Not to be
   embarked upon lightly: we'd need to be certain that we could make something
   really nice.
